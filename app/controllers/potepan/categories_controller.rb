class Potepan::CategoriesController < ApplicationController
  def show
    @taxon = Spree::Taxon.find(params[:id])
    @products = @taxon.products.includes(master: :prices)
    @taxonomies = Spree::Taxonomy.includes(root: :children)
    @product_count_by_taxon = Spree::Taxon.includes(:products).group(:id).count(:product_id)
  end
end
