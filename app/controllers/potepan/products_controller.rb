class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = Spree::Product.joins(:taxons).where.not(id: @product.id).where(spree_taxons: { id: @product.taxons }).distinct.limit(4)
  end
end
