require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do

  describe "GET #show" do
    let(:taxon) { create :taxon }

    it "assigns the requested taxon to @taxon" do
      get :show, params: { id: taxon.id }
      expect(assigns(:taxon)).to eq taxon
    end

    it "renders the :show template" do
      get :show, params: { id: taxon.id }
      expect(response).to render_template :show
    end
  end

end
