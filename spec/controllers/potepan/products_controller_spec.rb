require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do

  describe "GET #show" do
    let(:product) { create :product }
    let(:bag) { create(:taxon, name: "Bags") }
    let(:mug) { create(:taxon, name: "Mugs") }
    let(:bag_products) do
      create_list(:product, 10) do |product|
        product.taxons << bag
      end
    end

    let(:mug_products) do
      create_list(:product, 10) do |product|
        product.taxons << mug
      end
    end

    let(:bag_product) { bag_products.first }
    before do
      get :show, params: { id: product.id }
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "assigns the requested product to @product" do
      expect(assigns(:product)).to eq product
    end

    it "assigns the requested related_product to @related_product" do
      related_products = bag_products.to(4)
      get :show, params: { id: bag_product.id }
      expect(assigns(:related_products)).to match_array(related_products - [bag_product])
    end

    it "does not include products of other taxon" do
      mug_product = mug_products.first
      get :show, params: { id: mug_product.id }
      expect(assigns(:related_products)).to_not include(bag_products)
    end

    it "renders the :show template" do
      expect(response).to render_template :show
    end
  end

end

